import {Component, Input, OnInit} from '@angular/core';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import {Pokemon} from "../../models/pokemon.model";

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {
// One catalogue page consists of 30 pokemon
  pokemonsPerPage : number = 30;
  currentStart : number = 0;
  currentFilter : string = "";
  totalPokemon : number = 0;
  alphabet : string[] = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

   get pokemons(): Pokemon[] {
     let pokemon = this.pokemonCatalogueService.pokemons;
     if(this.currentFilter != "")
     {
       pokemon = this.pokemonCatalogueService.pokemons.filter(pokemon => pokemon.name[0] === this.currentFilter);
     }

     this.totalPokemon = pokemon.length;
     pokemon = pokemon.slice(this.currentStart, this.currentStart + this.pokemonsPerPage);
     const pokemonLeft = this.totalPokemon - this.pokemonsPerPage;
     console.log(`Current start ${this.currentStart}, pokemon left ${pokemonLeft}, total pokemon ${this.totalPokemon}`);
     return pokemon;
    }

   public moveToFirstPage(): Pokemon[]{
      this.currentStart = 0;
      return this.pokemons;
   }

   public resetFilter(){
    this.currentFilter = "";
    this.moveToFirstPage();
   }

   public setFilter(filter:string){
     this.currentFilter = filter;
     this.moveToFirstPage();
   }

  public nextPageAvailable() : boolean{
    return this.totalPokemon - this.currentStart - this.pokemonsPerPage > 0;
  }

   public prevPageAvailable() : boolean{
      return this.currentStart > 0;
   }

  // Proceed to next page if it exists
  public nextPokemons() : Pokemon[] {
    const pokemonLeft = this.totalPokemon - this.currentStart - this.pokemonsPerPage;
     console.log(`Pokemon left: ${pokemonLeft}`);
     if(pokemonLeft > 0){
       this.currentStart += this.pokemonsPerPage;
     }

     return this.pokemons;
  }

  // Return to previous page if it exists
  public prevPokemons() : Pokemon[] {
    this.currentStart -= this.pokemonsPerPage;
    if(this.currentStart < 0)
    {
      this.currentStart = 0;
    }

    return this.pokemons;
  }

  // Determine if we are on the first page in the catalogue
  // public isFirstPage() : boolean {
  //   return this.firstPage;
  // }

  // Determine if we are on the last page in the catalogue
  // public isLastPage() : boolean {
  //   return this.lastPage;
  // }

  get loading(): boolean {
    return this.pokemonCatalogueService.loading;
  }

  get error(): string {
    return this.pokemonCatalogueService.error;
  }

  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) { }

  ngOnInit(): void {
     if(this.pokemonCatalogueService.pokemons.length < 1){
       console.log("No pokemon fetched yet, doing it now");
       this.pokemonCatalogueService.findAllPokemons()
     }
  }

}

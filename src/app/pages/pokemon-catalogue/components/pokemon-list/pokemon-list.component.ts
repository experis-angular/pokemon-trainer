import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from "../../../../models/pokemon.model";

@Component({
  selector: 'pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {
  gridColumns = 5; // Default value for columns in catalogue
  @Input() pokemons: Pokemon[] = [];

  constructor() { }

  ngOnInit(): void {
  }
}

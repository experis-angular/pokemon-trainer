import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { ProfilePage } from './pages/profile/profile.page';
import { FormsModule } from '@angular/forms';
import { PokemonListComponent } from "./pages/pokemon-catalogue/components/pokemon-list/pokemon-list.component";
import { PokemonListItemComponent } from "./pages/pokemon-catalogue/components/pokemon-card/pokemon-list-item.component";
import { MatGridListModule } from '@angular/material/grid-list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatSliderModule} from "@angular/material/slider";
import {MatInputModule} from "@angular/material/input";
import {LoginFormComponent} from "./pages/login/login-form/login-form.component";
import {NavbarComponent} from "./pages/navbar/navbar.component";
import {CollectButtonComponent} from "./pages/pokemon-catalogue/collect-button/collect-button.component";

@NgModule({
  declarations: [ // Components
    AppComponent,
    LoginPage,
    PokemonCataloguePage,
    ProfilePage,
    LoginFormComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    NavbarComponent,
    CollectButtonComponent
  ],
  imports: [ // Modules
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    MatGridListModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    FlexLayoutModule,
    MatSliderModule,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

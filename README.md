# Pokemon Trainer
Pokemon Trainer is a Single Page Application where the user can browse a catalog of all the available Pokemon and add them to your collection.

## Description
The app is created with Angular and a private REST API. To use the site, user needs to enter their name which works as a unique login identifier. Name gets saved along with collected Pokemon to the database and can be accessed by reusing the same name afterwards. On the Catalogue page the user can add any Pokemon to their collection and visit their collection on the Trainer page. 

The app can be used here: https://tk-pokemon-trainer.herokuapp.com/

## Features
-   Create a user account with just a username
-   Login/Logout
-   Browse Pokemon Catalogue and add Pokemon to collection
-   Show collected Pokemon on Trainer page and remove Pokemon from the collection

## Libraries used
-   Angular Material
-   Angular Flex-Layout
-   Tailwind CSS
-   Animate.css

## Install:
```
-   To run the code on local machine you will need access to the API (environment.ts file) or create your own backend. 
-   You will also need Node.js: https://nodejs.org/en/download/, Angular CLI: https://angular.io/cli and git: https://git-scm.com/downloads
-   In your console navigate to the folder where you want the project and run the following commands:
    -   git clone https://gitlab.com/experis-angular/pokemon-trainer.git
    -   cd pokemon-trainer
-   For API access, create "environment.ts" and "environment.prod.ts" files in the src/environments/ folder of the project, add the following lines and paste correct API key and url inside the quotation marks:

environment.ts:
export const environment = {
  production: false,
  apiPokemons: "<pokemon api url>",
  apiUser: "<user api url>",
  apiKey: "<api key>",
};

environment.prod.ts:
export const environment = {
  production: true,
  apiPokemons: "<pokemon api url>",
  apiUser: "<user api url>",
  apiKey: "<api key>",
};
```
## Usage
-   After installation run "ng serve" in the command line and you can use the app in your browser at the following address: http://localhost:4200/ 
-   Enter a username to create account or to log back in.
-   Browse Pokemon catalogue and add your favorite Pokemon to your collection by clicking the collect button. Use "Previous" and "Next" links to change between pages.
-   Open Profile page from the top right corner to see the Pokemon you have collected and to remove them from your collection.

## Contributors
- Tuomo Kuusisto https://gitlab.com/pampula
- Thien Nguyen https://gitlab.com/thiniv2


## License
[MIT](https://choosealicense.com/licenses/mit/)
